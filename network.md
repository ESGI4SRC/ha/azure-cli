# Network

## Se connecter :

```powershell
az login
```

## Créer ressource :

```powershell
az group create --resource-group sciencesuNetwork --location francecentral
```

## Création du réseau frontend : 

```powershell
az network vnet create --resource-group sciencesuNetwork --name sciencesuVNet --address-prefix 10.0.0.0/16 --subnet-name sciencesuFrontendSubnet --subnet-prefix 10.0.1.0/24
```

## Création du réseau backend : 

```powershell
az network vnet subnet create --resource-group sciencesuNetwork --vnet-name sciencesuVNet --name sciencesuBackendSubnet --address-prefix 10.0.2.0/24
```

## Création de l'IP Public :  

```powershell
az network public-ip create --resource-group sciencesuNetwork --name sciencesuPublicIP
```

## Création de la VM :  

```powershell
az vm create --resource-group sciencesuNetwork --name sciencesu-front-01 --vnet-name sciencesuVNet --subnet sciencesuFrontendSubnet --nsg scienceusNSG --public-ip-address sciencesuPublicIP --image UbuntuLTS --admin-username sciencesu --ssh-key-values ./.ssh/id_rsa.pub
```

## Déallouer l'IP Public de la VM 

```powershell
az vm deallocate --resource-group sciencesuNetwork --name sciencesu-front-01
```

## Allouer une IP Public Statique à la VM

```powershell
az network public-ip update --resource-group sciencesuNetwork --name sciencesuPublicIP --allocation-method static
```

## Création groupe de sécurité

```powershell
az network nsg create --resource-group sciencesuNetwork --name sciencesuBackendNSG
```

## Ajout du groupe de sécurité au sous-reseau backend

```powershell
az network vnet subnet update --resource-group sciencesuNetwork --vnet-name sciencesuVNet --name sciencesuBackendSubnet --network-security-group sciencesuBackendNSG
```

## Ajout d'une règle qui autorise HTTP sur le groupe de sécurité pour le réseau frontend

```powershell
az network nsg rule create --resource-group sciencesuNetwork --nsg-name scienceusNSG --name HTTP --access allow --protocol tcp --direction Inbound --priority 200 --source-address-prefix "*" --source-port-range "*" --destination-address-prefix "*" --destination-port-range 80
```

## Connexion à la VM

```powershell
ssh sciencesu@<ip-vm>
```

## Installation de nginx sur la VM

```bash
sudo apt install nginx -y
```

## Test de connexion du serveur web

```powershell
curl <ip-vm>
```

## Ajout d'une règle qui autorise SSH sur le groupe de sécurité pour le réseau backend

```powershell
az network nsg rule create --resource-group sciencesuNetwork --nsg-name sciencesuBackendNSG --name SSH --access Allow --protocol tcp --direction Inbound --priority 100 --source-address-prefix 10.0.1.0/24 --source-port-range "*" --destination-address-prefix "*" --destination-port-range "22"
```

## Ajout d'une règle qui interdise tous le trafic sur le groupe de sécurité pour le réseau backend

```powershell
az network nsg rule create --resource-group sciencesuNetwork --nsg-name sciencesuBackendNSG --name DenyAll --access Deny --protocol tcp --direction Inbound --priority 300  --source-address-prefix "*" --source-port-range "*" --destination-address-prefix "*" --destination-port-range "*"
```
