# Azure CLI

## Se connecter :

```powershell
az login
```

## Créer ressource :

```powershell
az group create --location westeurope --resource-group sciences-u-labs
```

```powershell
az group create --resource-group sciencesuAvailabilityGroup --location francecentral
```

## Créer une vm :

### On utilisant sa propre clé (ne marche pas)

```powershell
az vm create --resource-group sciences-u-labs --name vm-01 --image Debian --admin-username sciencesu --ssh-key-values ./.ssh/id_rsa.pub
```

### En générant une clé ssh

```powershell
az vm create --resource-group sciences-u-labs --name vm-01 --image Debian --admin-username sciencesu --generate-ssh-keys
```

## Pour générer avec une taille (petite)

```powershell
--size Standard_B1ls
```
```powershell
az vm resize --resource-group sciences-u-labs --size Standard_B1ls
```

## Pour générer avec plusieurs disk

```powershell
az vm create --resource-group sciences-u-disk --name vm-01 --image Debian --admin-username sciencesu --generate-ssh-keys --size Standard_B1ls --data-disk-sizes-gb 256 128
```

## Pour attacher disk

```powershell
az vm disk attach --resource-group sciences-u-disk --vm-name vm-01 --name dataDisk --size-gb 128 --sku Premium_LRS --new
```

## Pour creer une VM avec cloud-init

```powershell
az vm create --resource-group sciences-u-init --name init-01 --image UbuntuLTS --admin-username sciencesu --generate-ssh-keys --custom-data .\cloud_init.yml
```

## Pour se connecter à la vm : 

### Obtenir l'ip public de la vm : 

```powershell
az vm list-ip-addresses
```


## Pour se connecter 

```powershell
ssh sciencesu@<ip-vm>
```

## Arrêt VM 

```powershell
az vm stop --resource-group sciences-u-labs --name vm-01
```

## Ouvrir port VM

```powershell
az vm open-port --port 80 --resource-group sciences-u-init --name init-01
```

## Créer un groupe à haute disponibilité

```powershell
az vm availability-set create --resource-group sciencesuAvailabilityGroup --name sciencesuAvailabilitySet --platform-fault-domain-count 2 --platform-update-domain-count 2
```
## Créer les VMs dans le groupe à haute disponibilité

```bash
for i in `seq 1 2`; do az vm create --resource-group sciencesuAvailabilityGroup --name VM-0$i --availability-set sciencesuAvailabilitySet --size Standard_B1ls --vnet-name sciencesuVnet --subnet sciencesuSubNet --image UbuntuLTS --admin-username sciencesu --generate-ssh-keys ; done
```

## Supprimer groupe de ressources 

/!\ Cela supprime l'intégralité des ressources dans le groupe de ressources.

```powershell
az group delete --resource-group sciences-u-labs --no-wait
```
