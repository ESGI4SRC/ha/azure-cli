# Création Scale Set

## Création groupe de ressources

```bash
az group create --resource-group sciencesuGroupScaleSet --location francecentral
```

## Création groupe de machines virtuelles identiques + Load Balancer (ScaleSet)

```bash
az vmss create --resource-group sciencesuGroupScaleSet --name sciencesuScaleSetDisks --image UbuntuLTS --custom-data cloud_init.yml --upgrade-policy-mode automatic --admin-user sciencesu --generate-ssh-keys --data-disk-sizes-gb 50
```

## Attacher un disk à un ScaleSet existant

```bash
az vmss disk attach --resource-group sciencesuGroupScaleSet --vmss-name sciencesuScaleSetDisks --size-gb 50
```

## Détacher un disk à un ScaleSet existant

```bash
az vmss disk detach --resource-group sciencesuGroupScaleSet --vmss-name sciencesuScaleSet --lun 0
```

## Liste des instances

```bash
az vmss list-instances --resource-group sciencesuGroupScaleSet --name sciencesuScaleSetDisks --output table
```

## Connexion aux instances

```bash
ssh sciencesu@<ip-load-balancer>:<port instance>
```
