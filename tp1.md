# TP 1

## Objectifs



Faire répondre une application web PHP basée sur une infrastructure HA Azure
(pas de base de données)



## Attentes



- Un groupe de ressources dédiés à ce travail
- Un répartiteur de charge en frontal avec une IP statique.
- Une sonde permettant de monitorer la santé de votre appli web (port 80)
- Un groupe de machines virtuelles (ScaleSet) disposant d'un disque supplémentaire de 20Go
- Les machines sont provisionnées grâce à un fichier de configuration de cloud-init.
- Mettre en place les groupes de sécurités permettant de sécuriser le réseau

## Les plus



- Ajouter une base de données managée et adapter l'application pour qu'elle l'utilise.

# Commandes 
