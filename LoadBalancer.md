# Création Load Balancer

On créer un ScaleSet à la mano.

## Création groupe de ressources

```bash
az group create --resource-group sciencesuLoadBalancer --location francecentral
```

## Création IP Public

```bash
az network public-ip create --resource-group sciencesuLoadBalancer --name sciencesuPublicIP
```

```mermaid
flowchart LR
Navigateur <--> IPPublic["IP Publique"]
```

## Création Load Balancer

```bash
az network lb create --resource-group sciencesuLoadBalancer --name sciencesuLB --frontend-ip-name sciencesuFrontendPool --backend-pool-name myBackendPool --public-ip-address sciencesuPublicIP
```

## Création d'une sonde pour le Load Balancer

```bash
az network lb probe create --resource-group sciencesuLoadBalancer --lb-name sciencesuLB --name HTTPHealthProbe --protocol tcp --port 80
```

```mermaid
flowchart LR
Navigateur <--> IPPublic["IP Publique"]
IPPublic <--> LB["Load Balancer"]
```

## Création d'une règle NAT pour le Load Balancer

```bash
az network lb rule create --resource-group sciencesuLoadBalancer --lb-name sciencesuLB --name HTTPRule --protocol tcp --frontend-port 80 --backend-port 80 --frontend-ip-name sciencesuFrontendPool --backend-pool-name myBackendPool --probe-name HTTPHealthProbe
```

## Création du réseau

```bash
az network vnet create --resource-group sciencesuLoadBalancer --name sciencesuVnet --subnet-name sciencesuSubnet
```

```mermaid
flowchart LR
Navigateur <--> IPPublic["IP Publique"]
IPPublic <-->|"Frontend"| LB["Load Balancer"]
LB <-->|"Backend"| reseaux["Réseau Privé"]
```

## Création du groupe de sécurité

```bash
az network nsg create --resource-group sciencesuLoadBalancer --name sciencesuNSG
```

## Création règle de sécurité

```bash
az network nsg rule create --resource-group sciencesuLoadBalancer --nsg-name sciencesuNSG --name HTTPNSGRule --priority 1001 --protocol tcp --destination-port-range 80
```

```mermaid
flowchart LR
Navigateur <--> IPPublic["IP Publique"]
IPPublic <-->|"Frontend"| LB["Load Balancer"]
LB <-->|"Backend"| reseaux["Réseau Privé Sécurisé"]
```

## Création des interfaces réseaux

```bash
for i in `seq 1 3`; do az network nic create --resource-group sciencesuLoadBalancer --name sciencesu-$i --vnet-name sciencesuVnet --subnet sciencesuSubnet --network-security-group sciencesuNSG --lb-name sciencesuLB --lb-address-pools myBackendPool; done
```

```mermaid
flowchart LR
Navigateur <--> IPPublic["IP Publique"]
IPPublic <-->|"Frontend"| LB["Load Balancer"]
LB <-->|"Backend"| reseaux["Réseau Privé Sécurisé"]
reseaux --- interface1
reseaux --- interface2
reseaux --- interface3
```

## Créer un groupe à haute disponibilité

```bash
az vm availability-set create --resource-group sciencesuLoadBalancer --name sciencesuAvailabilitySet
```

## Créer des VMs (avec cloud-init)

```bash
for i in `seq 1 3`; do az vm create --resource-group sciencesuLoadBalancer --name vm-0$i --availability-set sciencesuAvailabilitySet --nics sciencesu-$i --image UbuntuLTS --admin-username sciencesu --ssh-key-values id_rsa.pub  --ssh-key-values ./.ssh/id_rsa.pub --custom-data cloud_init.yml --no-wait; done
```

```mermaid
flowchart LR
Navigateur <--> IPPublic["IP Publique"]
IPPublic <-->|"Frontend"| LB["Load Balancer"]
LB <-->|"Backend"| reseaux["Réseau Privé Sécurisé"]
reseaux --- interface1
reseaux --- interface2
reseaux --- interface3
subgraph availability
vm1
vm2
vm3
end
interface1 --- vm1
interface2 --- vm2
interface3 --- vm3
```

## Tester la configuration 

```bash
curl <ip-public>
```

## Créer une panne

```bash
az network nic ip-config address-pool remove --resource-group sciencesuLoadBalancer --nic-name sciencesu-2 --ip-config-name ipConfig1 --lb-name sciencesuLB --address-pool myBackendPool
```

```mermaid
flowchart LR
Navigateur <--> IPPublic["IP Publique"]
IPPublic <-->|"Frontend"| LB["Load Balancer"]
LB <-->|"Backend"| reseaux["Réseau Privé Sécurisé"]
reseaux --- interface1
reseaux --- interface3
subgraph availability
vm1
vm2
vm3
end
interface1 --- vm1
interface3 --- vm3
```
