# Création Scale Set

## Création groupe de ressources

```bash
az group create --resource-group sciencesuGroupScaleSet --location francecentral
```

## Création groupe de machines virtuelles identiques + Load Balancer (ScaleSet)

```bash
az vmss create --resource-group sciencesuGroupScaleSet --name sciencesuScaleSet --image UbuntuLTS --custom-data cloud_init.yml --upgrade-policy-mode automatic --admin-user sciencesu --generate-ssh-keys
```

## Liste des instances

```bash
az vmss list-instances --resource-group sciencesuGroupScaleSet --name sciencesuScaleSet --output table
```

## Connexion aux instances

```bash
ssh sciencesu@<ip-load-balancer>:<port instance>
```

## Création de la règle NAT

```bash
az network lb rule create --resource-group sciencesuGroupScaleSet --name HTTP --lb-name sciencesuScaleSetLB --backend-pool-name sciencesuScaleSetLBBEPool --backend-port 80 --frontend-ip-name loadBalancerFrontEnd --frontend-port 80 --protocol tcp
```

## Ajouter des instances

```bash
az vmss scale --resource-group sciencesuGroupScaleSet --name sciencesuScaleSet --new-capacity 3
```

## Pour résumer

```mermaid
flowchart LR
Navigateur <--> LB
LB <--> ScaleSet["ScaleSet(VM1,VM2,...)"]
ScaleSet <--> SQL["SQL Managé"]
```
ou
```mermaid
flowchart LR
Navigateur <--> LB
LB <--> ScaleSet["ScaleSet(VM1,VM2,...)"]
ScaleSet <--> LB2["LB"]
LB2 <--> Availability["Availability (Mysql,Mysql2,...)"]
```
